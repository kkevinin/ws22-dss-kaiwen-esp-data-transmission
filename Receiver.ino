
#include <ESP8266WiFi.h>
#include <WifiEspNow.h>

void OnMessageRecv(const uint8_t _mac[WIFIESPNOW_ALEN], const uint8_t* _buf, size_t _count, void* arg) {
  // Serial.printf("FromMAC:%02X:%02X:%02X:%02X:%02X:%02XReceiverData\n", _mac[0], _mac[1], _mac[2], _mac[3], _mac[4], _mac[5]);
  String message = "";
  for (int i = 0; i < static_cast<int>(_count); i++) {
    message += String(static_cast<char>(_buf[i]));
  }
  Serial.println(message);
  if (String(message).substring(0,1) == "1") {
    digitalWrite(5,LOW);

  } else if (String(message).substring(0,1) == "2") {
    digitalWrite(5,HIGH);
  }
  if (String(message).substring(1,2) == "1") {
    digitalWrite(4,LOW);

  } else if (String(message).substring(1,2) == "2") {
    digitalWrite(4,HIGH);
  }
  if (String(message).substring(2,3) == "1") {
    digitalWrite(0,LOW);

  } else if (String(message).substring(2,3) == "2") {
    digitalWrite(0,HIGH);
  }
}

void setup(){
  pinMode(2, OUTPUT);
  digitalWrite(2,LOW);
  Serial.begin(9600);
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(0, OUTPUT);
  WifiEspNow.onReceive(OnMessageRecv, nullptr);

  WiFi.persistent(false);
  WiFi.mode(WIFI_AP);
  WiFi.disconnect();
  WiFi.softAP("ESPNOW", nullptr, 3);
  WiFi.softAPdisconnect(false);

  Serial.print("ThisdeviceMAC:");
  Serial.println(WiFi.softAPmacAddress());

  bool ok = WifiEspNow.begin();
  if (!ok) {
    Serial.println("WifiEspNowInitializationFailed");
    ESP.restart();
  }
}

void loop(){

}
