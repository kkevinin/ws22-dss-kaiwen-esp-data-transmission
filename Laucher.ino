
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <DHT.h>
#include <OneButton.h>
#include <WifiEspNow.h>
#include <SimpleTimer.h>

String radio;
String heat;
String light;
String switchtime;
String sj;
LiquidCrystal_I2C mylcd(0x27,20,4);
TinyGPSPlus gps;
SoftwareSerial gps_ss(12, 2);
DHT dht0(0, 11);
OneButton button16(16,false);
OneButton button13(13,false);
OneButton button14(14,false);
OneButton button15(15,false);
uint8_t PEER_4E11AE15FA6C[] = {0x4E, 0x11, 0xAE, 0x15, 0xFA, 0x6C};

SimpleTimer timer;

String z(int s) {
  if (s < 10) {
    sj = String("0") + String(String(s));

  } else {
    sj = String(s);

  }
  return sj;
}

void attachDoubleClick16() {
  radio = "2";
}

void attachClick16() {
  radio = "1";
}

void attachDoubleClick13() {
  heat = "2";
}

void attachClick13() {
  heat = "1";
}

void attachDoubleClick14() {
  light = "2";
}

void attachClick14() {
  light = "1";
}

void attachDoubleClick15() {
  switchtime = "Sz";
}

void attachClick15() {
  switchtime = "Wz";
}

bool sendMessage(String _data) {
  char _msg[100];
  uint8_t _len = snprintf(_msg, 100, "%s", _data);
  return WifiEspNow.send(PEER_4E11AE15FA6C, reinterpret_cast<const uint8_t*>(_msg), _len);
}

void Simple_timer_1() {
  if (dht0.readTemperature() < 19) {
    heat = "2";

  }
  if (analogRead(A0) < 500) {
    light = "2";

  }
  if (sendMessage(String(radio) + String(heat) + String(light))) {
    Serial.println("ok");
  } else {
    Serial.println("no");
  }
}

void setup(){
  Serial.begin(9600);
  Serial.println(WiFi.macAddress());
  radio = "";
  heat = "";
  light = "";
  switchtime = "Wz";
  sj = "";
  mylcd.init();
  mylcd.backlight();
  gps_ss.begin(9600);
   dht0.begin();
  pinMode(A0, INPUT);
  button16.attachDoubleClick(attachDoubleClick16);
  button16.attachClick(attachClick16);
  button13.attachDoubleClick(attachDoubleClick13);
  button13.attachClick(attachClick13);
  button14.attachDoubleClick(attachDoubleClick14);
  button14.attachClick(attachClick14);
  button15.attachDoubleClick(attachDoubleClick15);
  button15.attachClick(attachClick15);

  WiFi.persistent(false);
  WiFi.mode(WIFI_AP);
  WiFi.disconnect();
  WiFi.softAP("ESPNOW", nullptr, 3);
  WiFi.softAPdisconnect(false);

  Serial.print("ThisdeviceMAC:");
  Serial.println(WiFi.softAPmacAddress());

  bool ok = WifiEspNow.begin();
  if (!ok) {
    Serial.println("WifiEspNowInitializationFailed");
    ESP.restart();
  }

  ok = WifiEspNow.addPeer(PEER_4E11AE15FA6C);
  if (!ok) {
    Serial.println("WifiEspNow.addPeer() failed");
    ESP.restart();
  }
  timer.setInterval(100L, Simple_timer_1);

}

void loop(){
  while (gps_ss.available()) {
    if (gps.encode(gps_ss.read())) {
      if (gps.date.isValid()) {

      }

    }
  }
  mylcd.setCursor(1-1, 1-1);
  mylcd.print(String("Temp:") + String(dht0.readTemperature()) + String("C") + String(" ") + String("RH:") + String(dht0.readHumidity()) + String("%"));
  mylcd.setCursor(1-1, 2-1);
  mylcd.print(String("Lux:") + String(analogRead(A0)) + String("Lu"));
  mylcd.setCursor(1-1, 3-1);
  mylcd.print(String("Time:") + String(z(gps.time.hour() - 4)) + String(":") + String(z(gps.time.minute())) + String(":") + String(z(gps.time.second())) + String("  ") + String(switchtime) + String("  "));
  mylcd.setCursor(1-1, 4-1);
  mylcd.print(String("Date:") + String(gps.date.day()) + String("/") + String(gps.date.month()) + String("/") + String(gps.date.year()));

  button16.tick();
  button16.tick();
  button13.tick();
  button13.tick();
  button14.tick();
  button14.tick();
  button15.tick();
  button15.tick();
  timer.run();

}
