# WS22 DSS Kaiwen ESP data transmission

<img src="images/1Results_photo.jpg" width="600" height="300">


## Introduction

The project was inspired by ESP Now: Datenübertragung per Funk  Make Magazin 3/2022 S. 64
 (https://www.heise.de/select/make/2022/3/2208810541228397160)
Original Code Repo(https://github.com/Thomas-bastelt/Wenn-die-bessere-Haelfte-ruft)

The project from magazion required use ESP Now,which is a connectionless communication protocol developed by Espressif that features short packet transmission. By ESP Now can establishes a new wireless system that can still exchange data between two ESP boards without Waln, Bluetooth, or LoRa.
This is a small signal transmitter, which can transmit the signal of turning on and off the radio and turning on and off the heating (automatically turn on when the detection is below 19 degrees), and I added the function of switching lights (automatically when the detection is below a certain value turn on the lights) and the function of switching time.It has temperature and humidity sensors and photoresistors, and a GPS module that displays the time, which are displayed on an LED screen.

The Project thanks for the guidance of Thomas Adams and Enikő Viszló.  

---- 


## Description 

### Short info
- Wireless data transfer with ESP Now
- Automation and supervision of a workshop

### Checklist
- Time required: 2 days
- Costs: 30 Euro 

### Material
- NodeMCU ESP8266 two pieces
- -IC socket for 28 pins for port expanders
- Buttons for transmitter and receiver
- On/Off switch
- Power USB 5V
- LED indicator light
- Relay module 4-fold
- Receiver/relay
- Resistors
- LCD display,
- DHT22 Sensor 
- Photoresistance
- GPS module patch antenna
- Enclosures for transmitter and receiver
- MCP23017 port expander for more GPIOs (I didn't use it in this project, if the port is not enough, you can choose to use it)

### Programming software
- Arduino IED

----



## Steps  

### Configure ESP8266 environment in Arduino IDE   

- Download and install the latest version of Arduino IDE from the official website, at least the version number is 1.8
<img src="images/2_Arduino_IDE.png" width="600" height="200">

- Add ESP8266 development board
First enter the URL for the respective board manager under File/Preferences. 
For ESP32 boards, the URL for package version 2 is
```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

```
The address of the ESP8266 board is
```
http://arduino.esp8266.com/stable/package_esp8266com_index.json

```
Multiple URLs can be entered separated by a comma.It becomes clearer with a list of the additional board manager URLs.
The boards must then be integrated. You get to the board manager.

<img src="images/3_Board_manager.jpg" width="600" height="200">

Then install packages  "esp32 by Espressif Systems" or "esp8288 by ESP8266 Community".
Finally you can then select the board to use under Tools/Board. 


###  Installing DHT Libraries for ESP8266

First open Arduino IDE and go to Sketch > Include Library > Manage Libraries
Then Search for “DHT” on the Search box and install the DHT library from Adafruit.

<img src="images/4_DHT.png" width="300" height="300">

In the same way install Adafruit Unified Sensor.

Notice: After installing the libraries, restart your Arduino IDE.


###  Getting the Boards MAC Address

```
#include "WiFi.h"

void setup(){
  Serial.begin(9600);
  Serial.println(WiFi.macAddress());
  }
 
void loop(){

}

```
After uploading the code, the MAC address should be show on the Serial Monitor，remember it.

- Install the CH340 driver

- Download program to ESP8266

----



### ESP8266 Communication ESP-NOW Code 

- I had a big problem with this part because the GitHub code (https://github.com/Thomas-bastelt/Wenn-die-bessere-Haelfte-ruft) provided in the magazine never worked for me.There are various problems.


#### - In order to deal with various problems, I spent a lot of time learning code knowledge.
This tutorial helped me a lot(https://randomnerdtutorials.com/esp-now-two-way-communication-esp8266-nodemcu/)

- After learning and understanding the code, I found that the github code has a simpler coding method in some aspects, and I hope to add new functions, so I gave up using the code provided by github and wrote it according to the steps of the tutorial and the method of Github ,the I write the new code for the project.


#### - I changed most of the code and added additional functions, the following two files are all the code used in this project, completely coded by myself.You can see by comparing with the GitHub original code.

> https://gitlab.com/kkevinin/ws22-dss-kaiwen-esp-data-transmission/-/blob/main/Laucher.ino  
> https://gitlab.com/kkevinin/ws22-dss-kaiwen-esp-data-transmission/-/blob/main/Receiver.ino

- Finally after writing the code, compile program  and download it to ESP8266 for use

----



## Weekly progress

<img src="images/7_week.png" width="1010" height="100">

- 12.10.2022 - 18.10.2022
After studying several projects in magazine, I finally chose this project based on my interest, thinking that it is more practical and can be used on the construction site.
- 18.10.2022 - 30.10.2022
Learn arduino ide, ESP Now and buy materials
- 30.10.2022 - 15.11.2022
Configure the environment and follow the steps to use the original code in github.
- 15.11.2022 - 30.11.2022
Encountered a lot of problems.Can't read the circuit diagram, keep connecting wrong. Unable to successfully download the original code of Arduino IDE to ESP8266.
- 30.11.2022 - 20.12.2022
In order to deal with the problem and learn the entire coding process completely, try to modify the code.
Learn circuit diagrams and make hardware circuit boards for the first time.
- 10.01.2023 - 17.01.2023
Purchase hardware materials, add new functions, and still encounter many problems, unable to run successfully.
- 17.01.2023 - 30.01.2023
After learning, re-write all the complete code. Modified the connection method according to the tutorial.
- 30.01.2023 - 15.02.2023
Modify and debug, and finally succeed!
- 15.02.2023 - 28.02.2023
Make a simple enclosure and posters, understand and compare market-related products, and make Repo files.

----



## My Development part here  

- In the process of debugging, I changed 60% of the code and wrote the program of this project myself.
- Changing the power supply to 5V USB is more convenient than batteries.
- Added the function of measuring light intensity and the function of switching time.
- In the hardware, it can be seen that I have added a button to control the switch of the light, an indicator light to indicate whether the light is turned on by the receiver, and a button to control the switching time. In this way, the summer time Sz and winter time Wz can be displayed on the LED screen and switched. I added a photoresistor, and the light intensity can be obtained through the change of the voltage on both sides, and the light intensity can be displayed on the LED screen.


### The red circle in the picture is the content I added

<img src="images/5_myadd.png" width="700" height="400">

- I also added the corresponding code for these hardware parts.
  It can be found in many places in the code file.

> https://gitlab.com/kkevinin/ws22-dss-kaiwen-esp-data-transmission/-/blob/main/Laucher.ino  
> https://gitlab.com/kkevinin/ws22-dss-kaiwen-esp-data-transmission/-/blob/main/Receiver.ino

- Add when the light intensity is less than 500, it will automatically turn on the light.

```
  if (analogRead(A0) < 500) {
    light = "2";

```
```
  if (String(message).substring(2,3) == "1") {
    digitalWrite(0,LOW);

  } else if (String(message).substring(2,3) == "2") {
    digitalWrite(0,HIGH);
  }
}

```
----


## The OutPuts

- <img src="images/6_result.png" width="700" height="400">

- The temperature and humidity are got by the Sensor, and the time and date are got by the GPS module and displayed on the LED screen.
- Set up a wireless transmission system between ESP boards.
- Use indicator lights to represent radios, lights and heating, double-click to turn on, the lights are on, and click to turn off.
- Heating : Automatics shut down when temperature above 19 °C.


#### Personal innovation part
- Detect light intensity: Add a photoresistor to know the light intensity by detecting the voltage change on both sides.
- Automatically turn on the light when the detection light intensity is low.
Can switch between winter time and summer time for EU.


### You can watch the project demo video I recorded through this link.

----











